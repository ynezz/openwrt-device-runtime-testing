.testbed:
  stage: run
  image: "$CI_REGISTRY_IMAGE/testbed:latest"
  variables:
    DUT_SLEEP_AFTER_BOOT: 30
    DUT_SSH_CONNECT_TIMEOUT: 30
    DUT_SSH_CONNECT_ATTEMPTS: 3
    OWRT_DOWNLOADS_URL: https://downloads.openwrt.org
    TESTBED_MNG_INTERFACE: enp1s0
    TESTBED_LAN_INTERFACE: enp2s0
    TESTBED_WAN_INTERFACE: enp3s0
    TESTBED_TFTP_PATH: /var/lib/tftpboot
    TARGET_LAN_IP: 192.168.1.1
    TARGET_LAN_TEST_HOST: 192.168.1.2
    CRAM_REMOTE_COMMAND: ssh root@$TARGET_LAN_IP
    CRAM_TEST_SUITE: |
      .gitlab/tests/cram/generic/build/generic
      .gitlab/tests/cram/generic/build/$OWRT_IMAGE_BUILD_RELEASE
      .gitlab/tests/cram/generic/arch/$DUT_TARGET
      .gitlab/tests/cram/generic/arch/${DUT_TARGET}_$DUT_SUBTARGET
      .gitlab/tests/cram/generic/board/$DUT_BOARD
      .gitlab/tests/cram/generic/board/${DUT_BOARD}_$OWRT_IMAGE_BUILD_RELEASE
      .gitlab/tests/cram/$DUT_BOOT_MEDIUM/build/generic
      .gitlab/tests/cram/$DUT_BOOT_MEDIUM/build/$OWRT_IMAGE_BUILD_RELEASE
      .gitlab/tests/cram/$DUT_BOOT_MEDIUM/arch/$DUT_TARGET
      .gitlab/tests/cram/$DUT_BOOT_MEDIUM/arch/${DUT_TARGET}_$DUT_SUBTARGET
      .gitlab/tests/cram/$DUT_BOOT_MEDIUM/board/$DUT_BOARD
      .gitlab/tests/cram/$DUT_BOOT_MEDIUM/board/${DUT_BOARD}_$OWRT_IMAGE_BUILD_RELEASE

  before_script:
    - sudo ip link set $TESTBED_LAN_INTERFACE up 2> /dev/null
    - sudo ip link set $TESTBED_WAN_INTERFACE up 2> /dev/null
    - sleep 10

    - eval $(ssh-agent -s)
    - echo "$TESTBED_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh; chmod 700 ~/.ssh
    - >
      if echo "$CI_RUNNER_DESCRIPTION" | grep -q testbed-01; then
        eval $(ssh-agent -s)
        echo "$TESTBED_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
        ssh-keyscan $TESTBED_UART_RELAY_HOST > ~/.ssh/known_hosts 2> /dev/null
        chmod 644 ~/.ssh/known_hosts
      fi


    - >
      url="$OWRT_DOWNLOADS_URL/$(echo $OWRT_IMAGE_BUILD_RELEASE | tr '_' '/')/targets/$DUT_TARGET/$DUT_SUBTARGET";
      image=$(curl "$url/" | sed -nE "s/.*href\=\"($DUT_IMAGE_MATCH_PATTERN)\".*/\1/p");
      echo "Downloading $url/$image to $TESTBED_TFTP_PATH/$DUT_TFTP_IMAGE_FILENAME";
      curl --fail "$url/$image" > "$TESTBED_TFTP_PATH/$DUT_TFTP_IMAGE_FILENAME"

    - >
      if echo "$CI_RUNNER_DESCRIPTION" | grep -q testbed-02; then
        export TB_CONFIG=".testbed/labgrid/testbed-02.yaml"
      fi

    - .gitlab/scripts/testbed-device.py --target $LABGRID_TARGET boot_into shell
    - >
      .gitlab/scripts/testbed-device.py
      --target $LABGRID_TARGET check_network
      --network lan
      --remote-host $TARGET_LAN_TEST_HOST

    - echo "Waiting for SSH connection availability with ${DUT_SSH_CONNECT_TIMEOUT}sec timeout, with $DUT_SSH_CONNECT_ATTEMPTS attempts..."
    - time ssh -o BatchMode=yes -o StrictHostKeyChecking=no -o ConnectionAttempts=$DUT_SSH_CONNECT_ATTEMPTS -o ConnectTimeout=$DUT_SSH_CONNECT_TIMEOUT root@$TARGET_LAN_IP 'exit 0'

    - ssh-keyscan $TARGET_LAN_IP >> ~/.ssh/known_hosts 2> /dev/null
    - ssh root@$TARGET_LAN_IP "ubus call system board" | tee system-$LABGRID_TARGET.json

  script:
    - set -o pipefail
    - sleep $DUT_SLEEP_AFTER_BOOT
    - python3 -m cram --verbose $CRAM_TEST_SUITE $CRAM_TEST_SUITE_EXTRA | tee cram-result-$LABGRID_TARGET.txt

  after_script:
    - mkdir -p ~/.ssh; chmod 700 ~/.ssh
    - >
      if echo "$CI_RUNNER_DESCRIPTION" | grep -q testbed-01; then
        eval $(ssh-agent -s)
        echo "$TESTBED_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
        ssh-keyscan $TESTBED_UART_RELAY_HOST > ~/.ssh/known_hosts 2> /dev/null
        chmod 644 ~/.ssh/known_hosts
      fi

    - >
      if echo "$CI_RUNNER_DESCRIPTION" | grep -q testbed-02; then
        export TB_CONFIG=".testbed/labgrid/testbed-02.yaml"
      fi


    - ssh-keyscan $TARGET_LAN_IP >> ~/.ssh/known_hosts 2> /dev/null
    - >
      ssh root@$TARGET_LAN_IP exit && {
        ssh root@$TARGET_LAN_IP ps > processes-$LABGRID_TARGET.txt
        ssh root@$TARGET_LAN_IP dmesg > dmesg-$LABGRID_TARGET.txt
        ssh root@$TARGET_LAN_IP logread > logread-$LABGRID_TARGET.txt
        ssh root@$TARGET_LAN_IP cat /etc/config/network > uci-network-$LABGRID_TARGET.txt
        ssh root@$TARGET_LAN_IP cat /etc/config/wireless > uci-wireless-$LABGRID_TARGET.txt
      } || true

    - .gitlab/scripts/testbed-device.py --target $LABGRID_TARGET power off

    - mv console_$LABGRID_TARGET console_$LABGRID_TARGET.txt || true

  artifacts:
    expire_in: 1 month
    when: always
    paths:
      - .gitlab/tests/cram/**/*.t.err
      - processes-$LABGRID_TARGET.txt
      - dmesg-$LABGRID_TARGET.txt
      - logread-$LABGRID_TARGET.txt
      - uci-*-$LABGRID_TARGET.txt
      - system-$LABGRID_TARGET.json
      - console_$LABGRID_TARGET.txt
      - cram-result-$LABGRID_TARGET.txt

.testbed true.cz:
  extends: .testbed
  variables:
    TESTBED_UART_RELAY_HOST: uart-relay.testbed.vpn.true.cz
